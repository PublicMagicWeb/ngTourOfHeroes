import {
    Routes
} from '@angular/router';
import {
    RouterContainerMagicComponent
} from "@magic-xpa/angular";
export const routes: Routes = [{
        path: 'List',
        component: RouterContainerMagicComponent,
    },
    {
        path: 'Top',
        component: RouterContainerMagicComponent,
    },
    {
        path: 'Details/:p_id',
        component: RouterContainerMagicComponent,
    },
];