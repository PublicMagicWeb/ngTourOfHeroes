import {
    Component
} from '@angular/core';
import {
    TaskBaseMagicComponent,
    magicProviders
} from "@magic-xpa/angular";
@Component({
    selector: 'mga-HeroDetails',
    providers: [...magicProviders],
    styleUrls: ['./HeroDetails.component.css'],
    templateUrl: './HeroDetails.component.html'
}) export class HeroDetails extends TaskBaseMagicComponent {}