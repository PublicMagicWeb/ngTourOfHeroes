import {
    Component
} from '@angular/core';
import {
    BaseMatTableComponent,
    matProviders
} from "@magic-xpa/angular-material-core";
@Component({
    selector: 'mga-HeroesList',
    providers: [...matProviders],
    styleUrls: ['./HeroesList.component.css'],
    templateUrl: './HeroesList.component.html'
}) export class HeroesList extends BaseMatTableComponent {
    displayedColumns = ['col_id',
        'col_name',
        'col_select_edit',
        'col_select_del',
    ];
}