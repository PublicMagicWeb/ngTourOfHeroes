import {
    Component
} from '@angular/core';
import {
    TaskBaseMagicComponent,
    magicProviders
} from "@magic-xpa/angular";
@Component({
    selector: 'mga-TopHeroes',
    providers: [...magicProviders],
    styleUrls: ['./TopHeroes.component.css'],
    templateUrl: './TopHeroes.component.html'
}) export class TopHeroes extends TaskBaseMagicComponent {}