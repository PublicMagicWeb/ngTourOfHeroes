import { Dashboard as Dashboard_Dashboard } from './Home/Dashboard/Dashboard.component';
import { HeroDetails as HeroDetails_HeroDetails } from './Hero/HeroDetails/HeroDetails.component';
import { HeroesList as HeroesList_HeroesList } from './Hero/HeroesList/HeroesList.component';
import { TopHeroes as TopHeroes_TopHeroes } from './Hero/TopHeroes/TopHeroes.component';

export const title = "";

export const magicGenCmpsHash = {               Dashboard_Dashboard:Dashboard_Dashboard,
              HeroDetails_HeroDetails:HeroDetails_HeroDetails,
              HeroesList_HeroesList:HeroesList_HeroesList,
              TopHeroes_TopHeroes:TopHeroes_TopHeroes,
       
};

export const magicGenComponents = [ Dashboard_Dashboard,
HeroDetails_HeroDetails,
HeroesList_HeroesList,
TopHeroes_TopHeroes 
];
